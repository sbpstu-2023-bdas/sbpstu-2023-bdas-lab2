package pkcs1v15

import (
	"github.com/stretchr/testify/require"
	"testing"
)

var mockMessage = "mock"

func TestEncrypt(t *testing.T) {
	first, err := GenerateKeyPair()
	require.NoError(t, err)
	second, err := GenerateKeyPair()
	require.NoError(t, err)

	cr1, err := Encrypt(mockMessage, first.GetPublic())
	require.NoError(t, err)
	cr2, err := Encrypt(mockMessage, second.GetPublic())
	require.NoError(t, err)

	require.NotEqual(t, cr1, cr2)

	decr1, err := Decrypt(cr1, first.GetPrivate())
	require.NoError(t, err)
	decr2, err := Decrypt(cr2, second.GetPrivate())
	require.NoError(t, err)

	require.Equal(t, mockMessage, decr1)
	require.Equal(t, mockMessage, decr2)
}

func TestSign(t *testing.T) {
	first, err := GenerateKeyPair()
	require.NoError(t, err)
	second, err := GenerateKeyPair()
	require.NoError(t, err)

	ver1, err := Sign(mockMessage, first.GetPrivate())
	require.NoError(t, err)
	ver2, err := Sign(mockMessage, second.GetPrivate())
	require.NoError(t, err)

	require.NotEqual(t, ver1, ver2)

	require.NoError(t, Verify(mockMessage, ver1, first.GetPublic()))
	require.NoError(t, Verify(mockMessage, ver2, second.GetPublic()))

}
