package main

import (
	"go.uber.org/zap"
	"sbpstu-2023-bdas/sbpstu-2023-bdas-lab2/internal/pkcs1v15"
)

func main() {
	l, _ := zap.NewProduction()

	firstPair, err := pkcs1v15.GenerateKeyPair()
	if err != nil {
		l.Fatal("Error generating Alice's key pair", zap.Error(err))
	}

	secondPair, err := pkcs1v15.GenerateKeyPair()
	if err != nil {
		l.Fatal("Error generating Bob's key pair", zap.Error(err))
	}

	message := "Secret message for second"
	encryptedMessage, err := pkcs1v15.Encrypt(message, secondPair.GetPublic())
	if err != nil {
		l.Fatal("Error encrypting message", zap.Error(err))
	}

	decryptedMessage, err := pkcs1v15.Decrypt(encryptedMessage, secondPair.GetPrivate())
	if err != nil {
		l.Fatal("Error decrypting message", zap.Error(err))
	}

	l.Info("Original message", zap.String("message", message))
	l.Info("Encrypted message", zap.String("message", encryptedMessage))
	l.Info("Decrypted message", zap.String("message", decryptedMessage))

	signedMessage, err := pkcs1v15.Sign(message, firstPair.GetPrivate())
	if err != nil {
		l.Fatal("Error signing message", zap.Error(err))
	}

	if err = pkcs1v15.Verify(message, signedMessage, firstPair.GetPublic()); err != nil {
		l.Fatal("Signature verification failed", zap.Error(err))
		return
	}

	l.Info("Signature verified successfully!")
}
