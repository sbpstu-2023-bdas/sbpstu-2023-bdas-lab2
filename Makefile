.PHONY: deps
deps:
	go mod tidy

.PHONY: build
build:
	go build -o bin/main cmd/main.go

.PHONY: test
test:
	go test -cover ./...